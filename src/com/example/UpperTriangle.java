package com.example;

import java.util.Scanner;

public class UpperTriangle {
    int[][] array;
    int rows;
    int columns;
    public void initializeArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the Rows:- ");
        rows = scanner.nextInt();
        System.out.println("Enter the Columns:- ");
        columns = scanner.nextInt();
        array=new int[rows][columns];
        System.out.println("Enter the Elements:- ");
        for (int iterate = 0; iterate < rows; iterate++) {
            for (int iterate2 = 0; iterate2 < columns; iterate2++) {
                array[iterate][iterate2]=scanner.nextInt();
            }
        }
    }

    public int checkUpperTriangleMatrix(){
        boolean isUpper=false;
        for (int iterate = 1; iterate < rows; iterate++) {
            for (int iterate2 = 0; iterate2 < iterate; iterate2++) {
                if (array[iterate][iterate2] != 0) {
                    isUpper=false;
                } else {
                    isUpper=true;
                }
            }
        }
        if(isUpper==true) {
            return 1;
        }
        return 0;
    }
}
