import com.example.UpperTriangle;

public class TriangleMain {
    public static void main(String[] args) {
        UpperTriangle upperTriangle=new UpperTriangle();
        upperTriangle.initializeArray();
        if(upperTriangle.checkUpperTriangleMatrix()==1){
            System.out.println("Yes");
        }
        else{
            System.out.println("No");
        }
    }
}